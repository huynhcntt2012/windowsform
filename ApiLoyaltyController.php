<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiLoyaltyController extends Controller
{
	public function __construct()
    {
        
    }

    public function index(){
    	 return 'Api Loyalty';
    }

    public function login(Request $request){
        $name = '';
        $password = '';
        if($request->input('Username') == ''){
            $errors .= 'Parameter request [account] ';
            return 0;
        }else{
            $name = $request->input('Username');
        }
        if($request->input('Password') == ''){
            $errors .= 'Parameter request [password] ';
            return 0;
        }else{
            $password = md5($request->input('Password'));
        }
    	$checkLogin = DB::table('smarterp__users')->where(array('account'=>$name,'password'=>$password))->get();
        
        if(count($checkLogin) == 1){
    	   return 1;
        }
        return 0;
    }

    public function users(Request $request){
        $data = DB::table('smarterp__users')->get();
        return json_encode($data); 
    }

}