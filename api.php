<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*$params = [
    'as' => 'api::',
    'version' => 'v1',
    'domain' => 'http://192.168.1.3',//env('APP_DOMAIN'), // Notice we use the domain WITHOUT port number
    'namespace' => 'App\\Http\\Controllers',
];*/

$api = app('Dingo\Api\Routing\Router');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api->version('v1', function($api){
    $api->get('/logint',                         '\App\Http\Controllers\Apicontroller@login');
    $api->get('/login',                         '\App\Http\Controllers\Apicontroller@login');
    $api->get('/location',                      '\App\Http\Controllers\Apicontroller@location');
    $api->get('/searchlocation',                '\App\Http\Controllers\Apicontroller@searchlocation');
    $api->post('/checkinlocation',              '\App\Http\Controllers\Apicontroller@checkinlocation');
    $api->post('/notification',                 '\App\Http\Controllers\Apicontroller@notification');
    $api->get('/getemployeeofcompany/{id}',     '\App\Http\Controllers\Apicontroller@getemployeeofcompany');
    $api->get('/getmain/{id}',                  '\App\Http\Controllers\Apicontroller@getmain');
    $api->post('/updateavatar',                 '\App\Http\Controllers\Apicontroller@updateavatar');
    $api->post('/updateimage',                  '\App\Http\Controllers\Apicontroller@updateimage');
    $api->get('/getjobemployee',                '\App\Http\Controllers\Apicontroller@getjobemployee');
    $api->get('/getmessemployee',               '\App\Http\Controllers\Apicontroller@getmessemployee');
    $api->post('/updateJob',                    '\App\Http\Controllers\Apicontroller@updateJob');
    $api->get('/getusername',                   '\App\Http\Controllers\Apicontroller@getusername');
    $api->post('/changeuser',                   '\App\Http\Controllers\Apicontroller@changeuser');
    $api->get('/getimage',                      '\App\Http\Controllers\Apicontroller@getimage');
    $api->get('/getreport',                     '\App\Http\Controllers\Apicontroller@getreport');
    //api Loyalty
    $api->get('/Loyalty',                       '\App\Http\Controllers\Api\ApiLoyaltyController@index');
    $api->post('/Loyalty/Login',                '\App\Http\Controllers\Api\ApiLoyaltyController@login');
    $api->get('/Loyalty/Users',                 '\App\Http\Controllers\Api\ApiLoyaltyController@users');
});

