﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FWLoyalty.Design;

namespace FWLoyalty
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            string RequestData = "{'Username': '" + txtUsername.Text + "','Password':'" + txtPassword.Text + "'}";
            //GetRequest("http://localhost/voyager.git/api/Loyalty");
            IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("Username", txtUsername.Text),
                new KeyValuePair<string, string>("Password", txtPassword.Text)
            };

            Task<string> StrA = PostRequest("http://localhost/voyager.git/api/Loyalty/Login", queries);
            string x = await StrA;
            int Result = int.Parse(x);

            if(Result == 1)
            {
                frmMain frmMain = new frmMain();
                frmMain.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Login Failed");
            }
            
        }

        async static void GetRequest(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage repose = await client.GetAsync(url))
                {
                    using(HttpContent content = repose.Content)
                    {
                        string myContent = await content.ReadAsStringAsync();
                    }
                }
            }
        }

        public async Task<string> PostRequest(string url, 
            IEnumerable<KeyValuePair<string, string>> queries)
        {
            /*
            IEnumerable<KeyValuePair<string,string>> queries = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("",""),
                new KeyValuePair<string, string>("","")
            } 
            */
            HttpContent q = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage repose = await client.PostAsync(url,q))
                {
                    using (HttpContent content = repose.Content)
                    {
                        return await content.ReadAsStringAsync();
                    }
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
