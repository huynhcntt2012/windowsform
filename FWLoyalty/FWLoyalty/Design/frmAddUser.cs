﻿using PCSC;
using PCSC.Exceptions;
using PCSC.Monitoring;
using PCSC.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FWLoyalty.Design
{
    public partial class frmAddUser : Form
    {
        public frmAddUser()
        {
            InitializeComponent();
        }

        private void frmAddUser_Load(object sender, EventArgs e)
        {
            string[] readerNamex;
            using (var context = new SCardContext())
            {
                context.Establish(SCardScope.System);
                readerNamex = context.GetReaders();
                context.Release();
            }

            if (readerNamex == null || readerNamex.Length == 0)
            {
                Console.WriteLine("There are currently no readers installed.");
                return;
            }
            // Establish PC/SC context.
            using (var monitor = new SCardMonitor(new ContextFactory(), SCardScope.System))
            {
                // Point the callback function(s) to the anonymous & static defined methods below.
                monitor.CardInserted += (sender1, args0) => DisplayEvent("CardInserted", args0);
                //monitor.CardRemoved += (sender, args0) => DisplayEvent("CardRemoved", args0);
                //monitor.Initialized += (sender, args0) => DisplayEvent("Initialized", args0);
                //monitor.StatusChanged += StatusChanged;
                monitor.MonitorException += MonitorException;

                monitor.Start(readerNamex);

                // Keep the window open
            }
        }

        private static void DisplayEvent(string eventName, CardStatusEventArgs unknown)
        {
            Console.WriteLine(">> {0} Event for reader: {1}", eventName, unknown.ReaderName);
            Console.WriteLine("ATR: {0}", BitConverter.ToString(unknown.Atr ?? new byte[0]));
            Console.WriteLine("State: {0}\n", unknown.State);


            //Works fine here
        }
        private static void StatusChanged(object sender, StatusChangeEventArgs args)
        {
            //Console.WriteLine(">> StatusChanged Event for reader: {0}", args.ReaderName);
            //Console.WriteLine("ATR: {0}", BitConverter.ToString(args.Atr ?? new byte[0]));
            //Console.WriteLine("Last state: {0}\nNew state: {1}\n", args.LastState, args.NewState);
        }

        private static void MonitorException(object sender1, PCSCException ex)
        {

            Console.WriteLine("Monitor exited due an error:");
            Console.WriteLine(SCardHelper.StringifyError(ex.SCardError));
        }
    }
}
