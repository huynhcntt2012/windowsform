﻿using Newtonsoft.Json;
using PCSC;
using PCSC.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FWLoyalty.Design
{
    public partial class frmMain : Form
    {

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            
            var json = "";
            string url = "http://localhost/voyager.git/api/Loyalty/Users";

            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString(url);

            }
            
            dynamic account = JsonConvert.DeserializeObject(json);

            dataGird.DataSource = account;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmAddUser frm = new frmAddUser();
            using (var context = new SCardContext())
            {
                context.Establish(SCardScope.System);

                string[] readerNames = null;
                try
                {
                    // retrieve all reader names
                    readerNames = context.GetReaders();

                    // get the card status of each reader that is currently connected
                    
                    if (readerNames.Length == 0)
                    {
                        MessageBox.Show("not found devices");
                    }
                    else
                    {
                        frm.Show();
                    }
                }
                catch (Exception ex)
                {
                    if (readerNames == null)
                    {
                        MessageBox.Show("not found devices");
                    }
                }
                
            }
        }

        private void dataGird_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex  != -1)
            {
                DataGridViewRow row = dataGird.Rows[rowIndex];
                MessageBox.Show(row.Cells[1].Value.ToString());
                frmView frm = new frmView();
                frm.Show();
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGird.CurrentRow.Index;
            if(rowIndex != -1)
            {
                frmView frm = new frmView();
                frm.Show();
            }
        }

        private void btnLoyalty_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGird.CurrentRow.Index;
            if (rowIndex != -1)
            {
                frmLoyalty frm = new frmLoyalty();
                frm.Show();
            }
        }
    }
}
